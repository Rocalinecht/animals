
function nom_aleatoire(){
    var noms = [
        "Marcel", "Médor", "Flipper", "Babe", "Rintintin", 
        "Idéfix", "Balou", "Lassie", "Jolly Jumper"
    ];
    return noms[Math.floor(Math.random()*noms.length)];
}

function donner_la_vie()
{
    let nom = nom_aleatoire();
    let animal = new Animal(nom);
    //let animal = Animal.init(nom);
}

var bt = document.getElementById("donner_la_vie");
bt.addEventListener("click", donner_la_vie, false);

var bt = document.getElementById("carnivore");
bt.addEventListener("click", Carnivore, false);

var bt = document.getElementById("vegetarien");
bt.addEventListener("click", Vegetarien, false);

var bt = document.getElementById("vegetal");
bt.addEventListener("click", Vegetal, false);